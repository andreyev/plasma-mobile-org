---
title: "Plasma Mobile Gear ⚙ 22.09 is Out"
subtitle: "Updates in Plasma Mobile for June to September 2022"
SPDX-License-Identifier: CC-BY-4.0
date: 2022-09-27
author: Plasma Mobile Team
authors:
- SPDX-FileCopyrightText: 2022 Devin Lin <devin@kde.org>
- SPDX-FileCopyrightText: 2022 Bart De Vries <bart@mogwai.be>
- SPDX-FileCopyrightText: 2022 Alexey Andreyev <aa13q@ya.ru>
- SPDX-FileCopyrightText: 2022 Paul Brown <paul.brown@kde.org>
actiondrawer:
- name: Clear all Notifications button
  url: notifications-1.png
- name: Do not disturb quicksetting
  url: notifications-2.png
homescreen:
- name: Halcyon favorites page
  url: halcyon-1.png
- name: Halcyon applications page
  url: halcyon-2.png
- name: Configuration menu
  url: homescreen-1.png
- name: Containment configuration
  url: homescreen-2.png
dialer:
- name: Dialer answer swipe page
  url: dialer-answer-swipe.png
- name: Dialer call blocking page
  url: dialer-call-blocking.png
kasts:
- name: Podcast page
  url: kasts-1.png
- name: Episode page
  url: kasts-2.png
- name: Sleep timer
  url: kasts-3.png
kweather:
- name: Background rendered directly with OpenGL
  url: kweather-1.png
- name: New settings
  url: kweather-2.png
- name: Locations dialog
  url: kweather-3.png
- name: Settings dialog
  url: kweather-4.png
qmlkonsole:
- name: Translucent background
  url: qmlkonsole-1.png
- name: New settings
  url: qmlkonsole-2.png
- name: Tablet format
  url: qmlkonsole-3.png
- name: Tablet settings dialog
  url: qmlkonsole-4.png
settings:
- name: Home page
  url: settings-1.png
- name: System info
  url: settings-2.png
- name: Keyboard configuration
  url: settings-3.png
- name: Date and time configuration
  url: settings-4.png
- name: Energy settings
  url: settings-5.png
- name: Battery page
  url: settings-6.png
raven:
- name: Widescreen
  url: raven-1.png
- name: Narrow
  url: raven-2.png
- name: Settings
  url: raven-3.png
---

The Plasma Mobile team is happy to announce the developments integrated into Plasma Mobile between July-September 2022. In this report you can also read about the release of Plasma Mobile Gear ⚙ 22.09.

## Shell

Plasma 5.26 is currently in testing, and will be released on October 11th.

### Action Drawer 

Yari added a "Clear all Notifications" button to the notifications list to quickly clear notifications. He also added a "do not disturb" quicksetting to toggle on/off notifications that show up as popups.

Devin added more descriptive warnings in the mobile data quicksetting. These can now notify users if they don't have a SIM card inserted or don't have an APN configured.

{{< screenshots name="actiondrawer" >}}

### Navigation Bar

Aleix added support in KWin to report whether applications support bringing up the virtual keyboard themselves, and Devin integrated this API into the navigation bar: a keyboard toggle button now shows up if the current application does not support keyboards (useful for apps using XWayland).

Shown below is an electron app (Freetube) running in XWayland, which the system will detect as not having support for virtual keyboards.

<img src="keyboard-button.png" alt="Keyboard toggle in an X11 app" width=200px/>

### Task Switcher

Yari added a close all button -- note that it requires a second tap to confirm.

<img src="task-switcher-close-all.png" alt="Close all button in task switcher" width=200px/>

### Status Bar

Devin fixed the issue which showed two mobile data status icons in the status bar when you were connected to mobile data.

### Homescreen

Devin worked on Halcyon, the new default homescreen. Halcyon aims for simplicity and lets you use it with one hand. He also updated the design of the homescreen configuration, making it easier to switch between the old homescreen (Folio) and the new one.

{{< screenshots name="homescreen" >}}

### Other

Devin worked on allowing Plasma Mobile to be installed alongside the regular Plasma Desktop, streamlining the configuration needed. You now simply switch the "Global Theme" in Settings to "Plasma Mobile", and log into the Plasma Mobile session. He also changed the start command for Plasma Mobile from `kwinwrapper` to `startplasmamobile`.

Below is a video of a Surface Pro 3 running Plasma Mobile on Arch Linux. Note that there is still more work to be done to improve the tablet experience!

<div class="ratio ratio-21x9">
  <video src="surface-pro.mp4" controls autoplay=false class="embed-responsive-item" muted></video>
</div>

## Dialer

Alexey worked on the incoming call screen and also fixed bugs and regressions. The fixes he worked on are related to accepting calls, showing notifications and haptics feedback, switching call audio modes automatically and also CI/CD for checking the developer changes. Thanks to the review from Vlad and groundwork from Aleix to propose Wayland protocol extension and KWin support, Alexey implemented lock screen overlay support for the incoming call screen. Alexey also introduced a new swipe control to answer calls.

Michael introduced support for call-blocking of unknown numbers. This makes it possible to change notification logic according to the address book or call numbers matching a pattern.

Marco introduced a regression fix to support sleep inhibition when the call is active.

{{< screenshots name="dialer" >}}

<div class="ratio ratio-16x9">
  <video src="dialer-answer-swipe.mp4" controls autoplay=false class="embed-responsive-item" loop muted></video>
</div>

## Kasts

Devin simplified the interface by combining the podcast page with the podcast episode list page. He also worked on improving the podcast and episode info pages by adding a button toolbar (moving actions out of the app header), and reworking the information layout.

Bart added sleep timer functionality. He also implemented several small feature requests and bugfixes: he added the possibility to push all local episode states to the (gpodder) sync server, added an option to mark a custom amount of episodes as unplayed when adding a new podcast, added saving of the window position and size, and solved several UI bugs.

{{< screenshots name="kasts" >}}

## Weather

Han Young ported the weather backgrounds to use OpenGL directly, improving performance on lower-end devices.

Devin ported the settings to the new mobile form style. He also improved tablet support by having the locations list and settings show up as dialogs on larger displays. Devin also fixed issues with the dynamic view weather location not changing if it is removed.

{{< screenshots name="kweather" >}}

## Terminal (QMLKonsole)

Devin ported the settings to the new mobile form style. He also added the ability to select the font, change the font size, and also change the terminal background transparency. Devin also worked on improving Terminal for tablet usage, adding a tab bar and moving the settings to a dialog when there is enough space.

{{< screenshots name="qmlkonsole" >}}

## Settings

Devin ported several configuration modules to the new mobile form style. He also reworked the energy settings module to include more battery information.

{{< screenshots name="settings" >}}

## Raven (Mail Client)

Devin renewed work on a native mail client for Plasma Mobile, based on Carl's work with Pelikan -- a prototype for an Akonadi-enabled email client. Raven is currently functional as a basic email reader, and is planned to share components with Kalendar's upcoming mail support.

{{< screenshots name="raven" >}}

## Clock

Devin removed the distracting timer animation that loops around every second.

## Recorder

Devin ported the settings to the new mobile form style.

## NeoChat

A lot has happened in NeoChat. Notably, James Graham implemented support for configuring notifications on a per-room basis. As part of his GSoC project, Snehit added a space selector to the room list, which allows filtering the room list for rooms included in the selected space. James also reworked the timeline layout to make it look nicer when the window is very wide.
There have also been a ton of other fixes and improvements in translations, the layout and the stability of NeoChat.

## Contributing

Want to help with the development of Plasma Mobile? Take Plasma Mobile for a spin! Check out the [device support for each distribution](https://www.plasma-mobile.org/get/) and find the version which will work on your phone.

Our [documentation](https://invent.kde.org/plasma/plasma-mobile/-/wikis/Issue-Tracking) gives information on how and where to report issues. Also, consider joining our [Matrix channel](https://matrix.to/#/#plasmamobile:matrix.org), and let us know what you would like to work on!
